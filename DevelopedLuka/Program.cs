﻿using System.Collections.Concurrent;
using System.Linq;
using System.Numerics;

namespace DevelopedLuka
{
    internal class Program
    {
        static List<BigInteger> primes = new List<BigInteger> ();
        static BigInteger B = 502;
        static List<BigInteger> PowMod(BigInteger num, List<BigInteger> pows, List<bool> calc, BigInteger mod)
        {
            num %= mod;
            List<BigInteger> result = pows.Select(x => new BigInteger(1)).ToList();
            List<BigInteger> temp_pows = new(pows);
            while(temp_pows.Any(pow => pow != 0))
            {
                for (int i = 0; i < temp_pows.Count; i++)
                {
                    if (calc[i] && (temp_pows[i] & 1) == 1)
                        result[i] = (result[i] * num) % mod;
                    temp_pows[i] >>= 1;
                }

                num = (num * num) % mod;
            }

            return result;
        }

        static BigInteger NOD(BigInteger a, BigInteger b)
        {
            if(a < b)
            {
                (a, b) = (b, a);
            }
            while (b != 0)
                (a, b) = (b, a % b);

            return a;
        }

        static void FastFactor(ref BigInteger m, BigInteger prime)
        {
            List<BigInteger> pows = new();

            while(m % prime == 0)
            {
                pows.Add(prime);
                prime *= prime;
            }

            for (int i = pows.Count - 1; i >= 0; i--)
                if (m % pows[i] == 0) m /= pows[i];
        }

        static int DevelopedLuka(BigInteger m)
        {
            if (m <= primes.Last() && primes.Contains(m))
                return 1;

            List<BigInteger> prime_list = new List<BigInteger>();
            BigInteger r = m - 1;

            foreach (var prime in primes)
            {
                if (m % prime == 0)
                    return 0;
                if (r % prime == 0)
                {
                    prime_list.Add(prime);
                    FastFactor(ref r, prime);
                }
                if (r == 1)
                    break;
            }

            BigInteger f = (m - 1) / r;

            BigInteger maxA = Convert.ToUInt64(Math.Pow(Math.Log((double)m), 2));
            List<bool> finded = prime_list.Select(x => false).ToList();
            List<bool> calcs = [true, .. finded.Select(x =>  true), r != 1];

            finded.Add(r == 1);

            var pows = new List<BigInteger> { m - 1 };
            pows.AddRange(prime_list.Select(x => (m - 1) / x));
            pows.Add(f);

            List<Dictionary<BigInteger, BigInteger>> primPows = pows.Select(x => new Dictionary<BigInteger, BigInteger>()).ToList();

            for (BigInteger a = 2; a <= maxA; a++)
            {
                if (m % a == 0)
                    return 0;

                List<BigInteger> aInPows = pows.Select(x => new BigInteger(1)).ToList();
                BigInteger tempA = a;

                foreach(var prime in primPows.First().Keys)
                {
                    int count = 0;
                    while (tempA % prime == 0)
                    {
                        tempA /= prime;
                        count++;
                    }
                    for (int i = 0; i < aInPows.Count; i++)
                    {
                        if (calcs[i])
                        {
                            BigInteger primeInPow = primPows[i][prime], aInPow = aInPows[i];
                            for (int j = 0; j < count; j++)
                                aInPow = (aInPow * primeInPow) % m;

                            aInPows[i] = aInPow;
                        }
                    }
                }

                if (tempA != 1)
                {
                    List<BigInteger> allPows = PowMod(tempA, pows, calcs, m);
                    for (int i = 0; i < aInPows.Count; i++)
                    {
                        if (calcs[i])
                        {
                            primPows[i].Add(tempA, allPows[i]);
                            aInPows[i] = (aInPows[i] * allPows[i]) % m;
                        }
                    }
                }
                if (aInPows[0] != 1)
                    return 0;

                for (int i = 0; i < prime_list.Count; i++)
                {
                    if (!finded[i])
                    {
                        if (aInPows[i + 1] != 1)
                        {
                            if (NOD(aInPows[i + 1] - 1, m) == 1)
                            {
                                finded[i] = true;
                                calcs[i + 1] = false;
                            }
                            else
                                return 0;
                        }
                    }
                }

                if (!finded.Last())
                {
                    if (aInPows.Last() != 1)
                    {
                        if (NOD(aInPows.Last() - 1, m) == 1)
                        {
                            finded[finded.Count - 1] = true;
                            calcs[calcs.Count - 1] = false;
                        }
                        else
                            return 0;
                    }
                }

                if (finded.All(x => x))
                    break;
            }

            if (finded.Any(x => !x))
                return 0;

            if (r == 1)
                return 1;

            if ((double)(f * f) >= Math.Sqrt((double)m))
            {
                if (m % f == 1)
                    return 1;
                else
                    return 0;
            }

            if ((1 + f * B) * (1 + f * B) > m)
            {
                return 1;
            }

            return -1;
        }     

        static void Main(string[] args)
        {
            List<BigInteger> all_primes = new();
            using (StreamReader sr = new StreamReader("real_primes.txt"))
            {
                all_primes = sr.ReadLine().Split(' ').Take(200000000).Select(x => new BigInteger(Convert.ToUInt64(x))).ToList();
                primes = all_primes.Take(int.Parse(args[0])).ToList();
                B = primes.Last() - 1;
                primes.Remove(primes.Last());
            }

            BigInteger primes_finded = 0;
            uint primes_question = 0;
            int result;

            DateTime start = DateTime.Now;
            using (StreamWriter sw = new(args[3]))
                for (BigInteger i = BigInteger.Parse(args[1]);
                               i <= BigInteger.Parse(args[2]); i += 2)
                {
                    if ((result = DevelopedLuka(i)) != 0) primes_finded++;
                    sw.Write(result + " ");
                }

            DateTime end = DateTime.Now;

            Console.WriteLine($"Затрачено {(end - start).TotalMilliseconds} миллисекунд");
            Console.WriteLine(primes_finded);
        }
    }
}
